package uz.atmdt.Husniddin;

import io.vertx.core.AbstractVerticle;

public class MainVerticle extends AbstractVerticle {
    @Override
    public  void start() throws Exception{
        vertx.createHttpServer()
                .requestHandler(
                        routerContext->routerContext
                            .response().end("Salom Husniddin")
                ).listen(8080);
    }

}
